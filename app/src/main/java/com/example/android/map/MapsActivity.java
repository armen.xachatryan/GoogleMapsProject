package com.example.android.map;

import android.Manifest;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    List<MarkerInfo> markers;
    String json;
    MediaPlayer player;
    int length;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        json = loadJSONFromAsset();
        Type listType = new TypeToken<ArrayList<MarkerInfo>>() {
        }.getType();
        markers = (ArrayList<MarkerInfo>) ConvertToObject(json, listType);


    }

    public void setMarker(MarkerInfo info) {

        MarkerOptions options = new MarkerOptions().title(info.getTitle()).position(new LatLng(info.getLat(), info.getLng())).snippet(info.getInformation() + info.getPlayer());
        mMap.addMarker(options);

    }

    public String loadJSONFromAsset() {
        String json = "";
        try {

            InputStream is = getAssets().open("test.json");

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer);


        } catch (IOException ex) {
        }
        return json;
    }


    public static <T> T ConvertToObject(String json, Type type) {
        Gson gson = new Gson();
        return gson.fromJson(json, type);
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        mMap = googleMap;


        for (int i = 0; i < markers.size(); ++i) {
            setMarker(markers.get(i));
        }

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(final Marker marker) {

                marker.showInfoWindow();


                final Button play =  findViewById(R.id.play);
                final Button pause =  findViewById(R.id.pause);

                play.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String[] s = marker.getSnippet().split("\\.");
                        int resourceID = getResources().getIdentifier(s[s.length - 1], "raw", getPackageName());
                        player = MediaPlayer.create(MapsActivity.this, resourceID);

                        if(pause.getText().equals("PAUSE")) {
                            player.start();
                            play.setEnabled(false);
                            return;
                        }

                        if(pause.getText().equals("Reset")) {
                            player.seekTo(length);
                            player.start();
                            play.setEnabled(false);
                            pause.setText("PAUSE");
                        }
                    }
                });

                pause.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (player.isPlaying()) {
                            if (pause.getText().equals("PAUSE")) {
                                player.pause();
                                play.setEnabled(true);
                                length = player.getCurrentPosition();
                                pause.setText("Reset");
                                return;
                            }

                        }
                        if (pause.getText().equals("Reset")) {
                            player.seekTo(0);
                            play.setEnabled(true);
                            pause.setText("PAUSE");
                        }
                    }
                });

                return false;

            }
        });


        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }


            @Override
            public View getInfoContents(Marker marker) {
                View v = LayoutInflater.from(MapsActivity.this).inflate(R.layout.marker_info, null);

                TextView location =  v.findViewById(R.id.title);
                TextView info =  v.findViewById(R.id.info);

                location.setText(marker.getTitle());
                info.setText(marker.getSnippet().substring(0, marker.getSnippet().lastIndexOf(".")+1));


                return v;
            }
        });

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(40.183333,44.516667),12));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
    }

}
