package com.example.android.map;

/**
 * Created by Armen on 19.02.2018.
 */

public class MarkerInfo {

    private String title;
    private String information;
    private double lat;
    private double lng;
    private String player;

    public String getPlayer() {
        return player;
    }

    public double getLat() {return lat; }

    public double getLng() {
        return lng;
    }

    public String getTitle() {
        return title;
    }

    public String getInformation() {
        return information;
    }


}
